﻿namespace VicMobilier_ACB.DashBoard
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.filterPanel = new System.Windows.Forms.Panel();
            this.closedDateRange = new VicMobilier_ACB.DashBoard.Controls.DateRangeControl();
            this.shippedDateRange = new VicMobilier_ACB.DashBoard.Controls.DateRangeControl();
            this.packedDateRange = new VicMobilier_ACB.DashBoard.Controls.DateRangeControl();
            this.plantComboBox = new System.Windows.Forms.ComboBox();
            this.plantViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orderNumberTextBox = new System.Windows.Forms.TextBox();
            this.statusesCheckedListBox = new VicMobilier_ACB.DashBoard.Controls.StatusCheckedListBox();
            this.orderNumberLabel = new System.Windows.Forms.Label();
            this.plantLabel = new System.Windows.Forms.Label();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.refreshToolButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.viewPackagingToolButton = new System.Windows.Forms.ToolStripButton();
            this.viewShippingToolButton = new System.Windows.Forms.ToolStripButton();
            this.closeShippingToolButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.newShippingToolButton = new System.Windows.Forms.ToolStripButton();
            this.orderDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.plantNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paletsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boxesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastDatePackedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastDateShippedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateClosedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderDetailViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.filterPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plantViewModelBindingSource)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailViewModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // filterPanel
            // 
            this.filterPanel.Controls.Add(this.closedDateRange);
            this.filterPanel.Controls.Add(this.shippedDateRange);
            this.filterPanel.Controls.Add(this.packedDateRange);
            this.filterPanel.Controls.Add(this.plantComboBox);
            this.filterPanel.Controls.Add(this.orderNumberTextBox);
            this.filterPanel.Controls.Add(this.statusesCheckedListBox);
            this.filterPanel.Controls.Add(this.orderNumberLabel);
            this.filterPanel.Controls.Add(this.plantLabel);
            this.filterPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.filterPanel.Location = new System.Drawing.Point(0, 0);
            this.filterPanel.Name = "filterPanel";
            this.filterPanel.Size = new System.Drawing.Size(984, 85);
            this.filterPanel.TabIndex = 0;
            // 
            // closedDateRange
            // 
            this.closedDateRange.DateCaption = "Date terminé";
            this.closedDateRange.FromDate = null;
            this.closedDateRange.Location = new System.Drawing.Point(257, 55);
            this.closedDateRange.Name = "closedDateRange";
            this.closedDateRange.Size = new System.Drawing.Size(403, 29);
            this.closedDateRange.TabIndex = 7;
            this.closedDateRange.ToDate = null;
            // 
            // shippedDateRange
            // 
            this.shippedDateRange.DateCaption = "Date expédition";
            this.shippedDateRange.FromDate = null;
            this.shippedDateRange.Location = new System.Drawing.Point(257, 27);
            this.shippedDateRange.Name = "shippedDateRange";
            this.shippedDateRange.Size = new System.Drawing.Size(403, 29);
            this.shippedDateRange.TabIndex = 6;
            this.shippedDateRange.ToDate = null;
            // 
            // packedDateRange
            // 
            this.packedDateRange.DateCaption = "Date emballage";
            this.packedDateRange.FromDate = null;
            this.packedDateRange.Location = new System.Drawing.Point(257, 0);
            this.packedDateRange.Name = "packedDateRange";
            this.packedDateRange.Size = new System.Drawing.Size(403, 29);
            this.packedDateRange.TabIndex = 5;
            this.packedDateRange.ToDate = null;
            // 
            // plantComboBox
            // 
            this.plantComboBox.DataSource = this.plantViewModelBindingSource;
            this.plantComboBox.DisplayMember = "Name";
            this.plantComboBox.FormattingEnabled = true;
            this.plantComboBox.Location = new System.Drawing.Point(67, 4);
            this.plantComboBox.Name = "plantComboBox";
            this.plantComboBox.Size = new System.Drawing.Size(179, 21);
            this.plantComboBox.TabIndex = 4;
            this.plantComboBox.ValueMember = "ID";
            // 
            // plantViewModelBindingSource
            // 
            this.plantViewModelBindingSource.DataSource = typeof(VicMobilier_ACB.ViewModels.PlantViewModel);
            // 
            // orderNumberTextBox
            // 
            this.orderNumberTextBox.Location = new System.Drawing.Point(67, 31);
            this.orderNumberTextBox.Name = "orderNumberTextBox";
            this.orderNumberTextBox.Size = new System.Drawing.Size(179, 20);
            this.orderNumberTextBox.TabIndex = 3;
            // 
            // statusesCheckedListBox
            // 
            this.statusesCheckedListBox.BackColor = System.Drawing.SystemColors.Control;
            this.statusesCheckedListBox.CheckOnClick = true;
            this.statusesCheckedListBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.statusesCheckedListBox.FormattingEnabled = true;
            this.statusesCheckedListBox.Items.AddRange(new object[] {
            "Emballé",
            "Expédié",
            "Sous-traitant",
            "Partiellement expédié",
            "Terminé"});
            this.statusesCheckedListBox.Location = new System.Drawing.Point(666, 0);
            this.statusesCheckedListBox.Margin = new System.Windows.Forms.Padding(5);
            this.statusesCheckedListBox.MultiColumn = true;
            this.statusesCheckedListBox.Name = "statusesCheckedListBox";
            this.statusesCheckedListBox.Size = new System.Drawing.Size(318, 85);
            this.statusesCheckedListBox.TabIndex = 2;
            // 
            // orderNumberLabel
            // 
            this.orderNumberLabel.AutoSize = true;
            this.orderNumberLabel.Location = new System.Drawing.Point(4, 34);
            this.orderNumberLabel.Name = "orderNumberLabel";
            this.orderNumberLabel.Size = new System.Drawing.Size(60, 13);
            this.orderNumberLabel.TabIndex = 1;
            this.orderNumberLabel.Text = "Commande";
            // 
            // plantLabel
            // 
            this.plantLabel.AutoSize = true;
            this.plantLabel.Location = new System.Drawing.Point(4, 7);
            this.plantLabel.Name = "plantLabel";
            this.plantLabel.Size = new System.Drawing.Size(34, 13);
            this.plantLabel.TabIndex = 0;
            this.plantLabel.Text = "Usine";
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshToolButton,
            this.toolStripSeparator2,
            this.viewPackagingToolButton,
            this.viewShippingToolButton,
            this.closeShippingToolButton,
            this.toolStripSeparator1,
            this.newShippingToolButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 85);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(984, 31);
            this.mainToolStrip.TabIndex = 1;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // refreshToolButton
            // 
            this.refreshToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshToolButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshToolButton.Image")));
            this.refreshToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolButton.Name = "refreshToolButton";
            this.refreshToolButton.Size = new System.Drawing.Size(28, 28);
            this.refreshToolButton.Text = "Rafraîchir";
            this.refreshToolButton.Click += new System.EventHandler(this.refreshToolButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // viewPackagingToolButton
            // 
            this.viewPackagingToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.viewPackagingToolButton.Image = ((System.Drawing.Image)(resources.GetObject("viewPackagingToolButton.Image")));
            this.viewPackagingToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.viewPackagingToolButton.Name = "viewPackagingToolButton";
            this.viewPackagingToolButton.Size = new System.Drawing.Size(28, 28);
            this.viewPackagingToolButton.Text = "Visualiser emballage";
            this.viewPackagingToolButton.Click += new System.EventHandler(this.viewPackagingToolButton_Click);
            // 
            // viewShippingToolButton
            // 
            this.viewShippingToolButton.BackColor = System.Drawing.SystemColors.Control;
            this.viewShippingToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.viewShippingToolButton.Image = ((System.Drawing.Image)(resources.GetObject("viewShippingToolButton.Image")));
            this.viewShippingToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.viewShippingToolButton.Name = "viewShippingToolButton";
            this.viewShippingToolButton.Size = new System.Drawing.Size(28, 28);
            this.viewShippingToolButton.Text = "Visualiser";
            this.viewShippingToolButton.ToolTipText = "Visualiser expédition";
            this.viewShippingToolButton.Click += new System.EventHandler(this.viewShippingToolButton_Click);
            // 
            // closeShippingToolButton
            // 
            this.closeShippingToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.closeShippingToolButton.Image = ((System.Drawing.Image)(resources.GetObject("closeShippingToolButton.Image")));
            this.closeShippingToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeShippingToolButton.Name = "closeShippingToolButton";
            this.closeShippingToolButton.Size = new System.Drawing.Size(28, 28);
            this.closeShippingToolButton.Text = "Terminer";
            this.closeShippingToolButton.ToolTipText = "Terminer expédition";
            this.closeShippingToolButton.Click += new System.EventHandler(this.closeShippingToolButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // newShippingToolButton
            // 
            this.newShippingToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newShippingToolButton.Image = ((System.Drawing.Image)(resources.GetObject("newShippingToolButton.Image")));
            this.newShippingToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newShippingToolButton.Name = "newShippingToolButton";
            this.newShippingToolButton.Size = new System.Drawing.Size(28, 28);
            this.newShippingToolButton.Text = "Ajouter";
            this.newShippingToolButton.ToolTipText = "Ajouter expédition";
            this.newShippingToolButton.Click += new System.EventHandler(this.newShippingToolButton_Click);
            // 
            // orderDetailDataGridView
            // 
            this.orderDetailDataGridView.AllowUserToAddRows = false;
            this.orderDetailDataGridView.AllowUserToDeleteRows = false;
            this.orderDetailDataGridView.AllowUserToOrderColumns = true;
            this.orderDetailDataGridView.AutoGenerateColumns = false;
            this.orderDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.plantNameDataGridViewTextBoxColumn,
            this.orderNumberDataGridViewTextBoxColumn,
            this.OrderStatus,
            this.paletsDataGridViewTextBoxColumn,
            this.boxesDataGridViewTextBoxColumn,
            this.lastDatePackedDataGridViewTextBoxColumn,
            this.lastDateShippedDataGridViewTextBoxColumn,
            this.dateClosedDataGridViewTextBoxColumn});
            this.orderDetailDataGridView.DataSource = this.orderDetailViewModelBindingSource;
            this.orderDetailDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderDetailDataGridView.Location = new System.Drawing.Point(0, 116);
            this.orderDetailDataGridView.Name = "orderDetailDataGridView";
            this.orderDetailDataGridView.ReadOnly = true;
            this.orderDetailDataGridView.Size = new System.Drawing.Size(984, 445);
            this.orderDetailDataGridView.TabIndex = 2;
            // 
            // plantNameDataGridViewTextBoxColumn
            // 
            this.plantNameDataGridViewTextBoxColumn.DataPropertyName = "PlantName";
            this.plantNameDataGridViewTextBoxColumn.HeaderText = "Usine";
            this.plantNameDataGridViewTextBoxColumn.Name = "plantNameDataGridViewTextBoxColumn";
            this.plantNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.plantNameDataGridViewTextBoxColumn.Width = 125;
            // 
            // orderNumberDataGridViewTextBoxColumn
            // 
            this.orderNumberDataGridViewTextBoxColumn.DataPropertyName = "OrderNumber";
            this.orderNumberDataGridViewTextBoxColumn.HeaderText = "Commande";
            this.orderNumberDataGridViewTextBoxColumn.Name = "orderNumberDataGridViewTextBoxColumn";
            this.orderNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.orderNumberDataGridViewTextBoxColumn.Width = 125;
            // 
            // OrderStatus
            // 
            this.OrderStatus.DataPropertyName = "OrderStatusText";
            this.OrderStatus.HeaderText = "Statut";
            this.OrderStatus.Name = "OrderStatus";
            this.OrderStatus.ReadOnly = true;
            this.OrderStatus.Width = 125;
            // 
            // paletsDataGridViewTextBoxColumn
            // 
            this.paletsDataGridViewTextBoxColumn.DataPropertyName = "Palets";
            this.paletsDataGridViewTextBoxColumn.HeaderText = "Palettes";
            this.paletsDataGridViewTextBoxColumn.Name = "paletsDataGridViewTextBoxColumn";
            this.paletsDataGridViewTextBoxColumn.ReadOnly = true;
            this.paletsDataGridViewTextBoxColumn.Width = 75;
            // 
            // boxesDataGridViewTextBoxColumn
            // 
            this.boxesDataGridViewTextBoxColumn.DataPropertyName = "Boxes";
            this.boxesDataGridViewTextBoxColumn.HeaderText = "Boîtes";
            this.boxesDataGridViewTextBoxColumn.Name = "boxesDataGridViewTextBoxColumn";
            this.boxesDataGridViewTextBoxColumn.ReadOnly = true;
            this.boxesDataGridViewTextBoxColumn.Width = 75;
            // 
            // lastDatePackedDataGridViewTextBoxColumn
            // 
            this.lastDatePackedDataGridViewTextBoxColumn.DataPropertyName = "LastDatePacked";
            this.lastDatePackedDataGridViewTextBoxColumn.HeaderText = "Date emballage";
            this.lastDatePackedDataGridViewTextBoxColumn.Name = "lastDatePackedDataGridViewTextBoxColumn";
            this.lastDatePackedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastDateShippedDataGridViewTextBoxColumn
            // 
            this.lastDateShippedDataGridViewTextBoxColumn.DataPropertyName = "LastDateShipped";
            this.lastDateShippedDataGridViewTextBoxColumn.HeaderText = "Date expédition";
            this.lastDateShippedDataGridViewTextBoxColumn.Name = "lastDateShippedDataGridViewTextBoxColumn";
            this.lastDateShippedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateClosedDataGridViewTextBoxColumn
            // 
            this.dateClosedDataGridViewTextBoxColumn.DataPropertyName = "DateClosed";
            this.dateClosedDataGridViewTextBoxColumn.HeaderText = "Date terminée";
            this.dateClosedDataGridViewTextBoxColumn.Name = "dateClosedDataGridViewTextBoxColumn";
            this.dateClosedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // orderDetailViewModelBindingSource
            // 
            this.orderDetailViewModelBindingSource.DataSource = typeof(VicMobilier_ACB.ViewModels.OrderDetailViewModel);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.orderDetailDataGridView);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.filterPanel);
            this.MinimumSize = new System.Drawing.Size(1000, 250);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tableau de bord";
            this.filterPanel.ResumeLayout(false);
            this.filterPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plantViewModelBindingSource)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailViewModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel filterPanel;
        private System.Windows.Forms.ComboBox plantComboBox;
        private System.Windows.Forms.TextBox orderNumberTextBox;
        private VicMobilier_ACB.DashBoard.Controls.StatusCheckedListBox statusesCheckedListBox;
        private System.Windows.Forms.Label orderNumberLabel;
        private System.Windows.Forms.Label plantLabel;
        private Controls.DateRangeControl packedDateRange;
        private Controls.DateRangeControl closedDateRange;
        private Controls.DateRangeControl shippedDateRange;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton viewShippingToolButton;
        private System.Windows.Forms.ToolStripButton closeShippingToolButton;
        private System.Windows.Forms.ToolStripButton newShippingToolButton;
        private System.Windows.Forms.DataGridView orderDetailDataGridView;
        private System.Windows.Forms.BindingSource orderDetailViewModelBindingSource;
        private System.Windows.Forms.BindingSource plantViewModelBindingSource;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.DataGridViewTextBoxColumn plantNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn paletsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn boxesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastDatePackedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastDateShippedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateClosedDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton viewPackagingToolButton;
        private System.Windows.Forms.ToolStripButton refreshToolButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}

