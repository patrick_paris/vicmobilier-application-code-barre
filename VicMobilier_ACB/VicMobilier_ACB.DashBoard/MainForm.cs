﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VicMobilier_ACB.Core;
using VicMobilier_ACB.ViewModels;

namespace VicMobilier_ACB.DashBoard
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            var plants = Program.PlantManager.GetList();
            plants.Insert(0, new ViewModels.PlantViewModel(new Plant() { ID = 0, Name = "[Toutes]" }));
            plantViewModelBindingSource.DataSource = plants;

            statusesCheckedListBox.Items.Clear();
            var names = Enum.GetNames(typeof(VicMobilier_ACB.OrderStatus));
            for (var i = 1; i <= names.Length; i++)
            {
                statusesCheckedListBox.Items.Add(((VicMobilier_ACB.OrderStatus)i).ToFriendlyString(), ((VicMobilier_ACB.OrderStatus)i) != VicMobilier_ACB.OrderStatus.Terminated);
            }
        }

        public void RefreshOrderItems()
        {
            var orderFilter = new OrderDetailFilter()
            {
                PlantID = (int)plantComboBox.SelectedValue,
                OrderNumber = orderNumberTextBox.Text
            };

            orderFilter.OrderStatuses = new List<OrderStatus>();
            foreach (var indice in statusesCheckedListBox.CheckedIndices)
                orderFilter.OrderStatuses.Add((OrderStatus)((int)indice + 1));

            orderFilter.PackedDateRange.FromDate = packedDateRange.FromDate;
            orderFilter.PackedDateRange.ToDate = packedDateRange.ToDate;

            orderFilter.ShippedDateRange.FromDate = shippedDateRange.FromDate;
            orderFilter.ShippedDateRange.ToDate = shippedDateRange.ToDate;

            orderFilter.ClosedDateRange.FromDate = closedDateRange.FromDate;
            orderFilter.ClosedDateRange.ToDate = closedDateRange.ToDate;

            orderDetailViewModelBindingSource.DataSource = Program.OrderManager.GetDetailList(orderFilter);
        }

        private void newShippingToolButton_Click(object sender, EventArgs e)
        {
            var plantName = new PlantSelectForm(Program.PlantManager, true).SelectPlantName(this);

            if (!string.IsNullOrEmpty(plantName))
            {
                using (var packingForm = new PackingForm(plantName, Program.PlantManager, Program.OrderManager))
                {
                    packingForm.ShowDialog(this);
                }
                RefreshOrderItems();
            }
        }

        private OrderDetailViewModel GetCurrentOrderItem()
        {
            if (orderDetailDataGridView.DataSource == null) return null;
            if (orderDetailDataGridView.CurrentRow == null) return null;

            return orderDetailDataGridView.CurrentRow.DataBoundItem as OrderDetailViewModel;
        }

        private void closeShippingToolButton_Click(object sender, EventArgs e)
        {
            var currentOrderItem = GetCurrentOrderItem();
            if (currentOrderItem == null) return;

            if (MessageBox.Show(this, "Marquer cette commande comme terminée même si\r\ncertains items ne sont pas expédiés ?", "Terminer commande", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Program.OrderManager.CloseOrder(currentOrderItem.OrderNumber);
                RefreshOrderItems();
            }
        }

        private void viewShippingToolButton_Click(object sender, EventArgs e)
        {
            var currentOrderItem = GetCurrentOrderItem();
            if (currentOrderItem == null) return;

            new ShippingForm(currentOrderItem.PlantName, Program.PlantManager, Program.OrderManager, currentOrderItem.OrderNumber).ShowDialog(this);
            RefreshOrderItems();
        }

        private void viewPackagingToolButton_Click(object sender, EventArgs e)
        {
            var currentOrderItem = GetCurrentOrderItem();
            if (currentOrderItem == null) return;

            new PackingForm(currentOrderItem.PlantName, Program.PlantManager, Program.OrderManager, currentOrderItem.OrderNumber).ShowDialog(this);
            RefreshOrderItems();
        }

        private void refreshToolButton_Click(object sender, EventArgs e)
        {
            RefreshOrderItems();
        }
    }
}
