﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VicMobilier_ACB.DashBoard.Controls
{
    public sealed class StatusCheckedListBox : CheckedListBox
    {
        public StatusCheckedListBox()
        {
            ItemHeight = 20;
        }
        public override int ItemHeight { get; set; }
    }
}
