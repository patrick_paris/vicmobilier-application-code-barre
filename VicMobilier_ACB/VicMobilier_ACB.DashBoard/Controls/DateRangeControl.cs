﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VicMobilier_ACB.DashBoard.Controls
{
    public partial class DateRangeControl : UserControl
    {
        public DateRangeControl()
        {
            InitializeComponent();

            SetNullValue(fromDatePicker);
            SetNullValue(toDatePicker);
        }

        private string _DateCaption;
        public string DateCaption
        {
            get
            {
                return _DateCaption;
            }
            set
            {
                _DateCaption = value;
                fromDateLabel.Text = string.Format("{0} de", _DateCaption);
            }
        }

        public DateTime? FromDate
        {
            get {
                if (fromDatePicker.Format != DateTimePickerFormat.Custom)
                    return fromDatePicker.Value.Date;
                else
                    return null;
            }
            set
            {
                if (value.HasValue)
                {
                    fromDatePicker.Format = DateTimePickerFormat.Short;
                    fromDatePicker.Value = value.Value;
                }
                else
                    SetNullValue(fromDatePicker);
            }
        }

        public DateTime? ToDate
        {
            get
            {
                if (toDatePicker.Format != DateTimePickerFormat.Custom)
                    return toDatePicker.Value.Date;
                else
                    return null;
            }
            set
            {
                if (value.HasValue)
                {
                    toDatePicker.Format = DateTimePickerFormat.Short;
                    toDatePicker.Value = value.Value;
                }
                else
                    SetNullValue(toDatePicker);
            }
        }

        private void SetNullValue(DateTimePicker control)
        {
            control.CustomFormat = " ";
            control.Format = DateTimePickerFormat.Custom;
        }

        private void datePicker_CloseUp(object sender, EventArgs e)
        {
            ((DateTimePicker)sender).Format = DateTimePickerFormat.Short;
        }

        private void datePicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Clear)
            {
                SetNullValue((DateTimePicker)sender);
            }
        }
    }
}
