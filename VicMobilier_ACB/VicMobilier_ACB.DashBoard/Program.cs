﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VicMobilier_ACB.DashBoard
{
    static class Program
    {
        public static PlantManager PlantManager;
        public static OrderManager OrderManager;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            PlantManager = new PlantManager("DefaultConnection");
            OrderManager = new OrderManager("DefaultConnection");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
