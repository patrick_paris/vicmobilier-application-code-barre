﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB.ViewModels
{
    public class OrderDetailViewModel
    {
        public string PlantName { get; set; }
        public string OrderNumber { get; set; }
        public int OrderStatus { get; set; }
        public string OrderStatusText { get { return ((OrderStatus)this.OrderStatus).ToFriendlyString(); } }
        public string Palets { get; set; }
        public string Boxes { get; set; }
        public DateTime LastDatePacked { get; set; }
        public DateTime? LastDateShipped { get; set; }
        public DateTime? DateClosed { get; set; }
    }
}
