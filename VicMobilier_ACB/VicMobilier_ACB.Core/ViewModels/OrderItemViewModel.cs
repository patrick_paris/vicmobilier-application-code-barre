﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB.ViewModels
{
    public class OrderItemViewModel : BaseViewModel
    {
        public OrderItemViewModel(OrderItem orderItem)
        {
            Model = orderItem;
        }

        public bool Selected { get; set; }

        public bool Scanned
        {
            get
            {
                return this.OrderItem.DateScanned != null;
            }
        }

        public DateTime? DateShipped
        {
            get
            {
                return this.OrderItem.DateShipped;
            }
        }

        private OrderItem OrderItem
        {
            get { return Model as OrderItem; } 
        }

        public string BarCode
        {
            get
            {
                return this.OrderItem.BarCode;
            }
        }

        public string OrderItemType
        {
            get
            {
                switch (this.OrderItem.ItemType)
                {
                    case VicMobilier_ACB.OrderItemType.Palet:
                        return "Palette";
                    case VicMobilier_ACB.OrderItemType.Box:
                        return "Boîte";
                    default:
                        return string.Empty;
                }
            }
        }
    }

    public class OrderSummaryViewModel
    {
        public string PlantName { get; set; }
        public string OrderNumber { get; set; }
        public string OrderItemType { get; set; }
        public int PackedCount { get; set; }
        public int ScannedCount { get; set; }
    }
}
