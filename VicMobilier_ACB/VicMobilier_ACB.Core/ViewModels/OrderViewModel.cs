﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB.ViewModels
{
    public class OrderViewModel : BaseViewModel
    {
        public OrderViewModel(Order order)
        {
            Model = order;
            OrderItems = new List<OrderItemViewModel>();
        }

        public IList<OrderItemViewModel> OrderItems { get; set; }

        public Order Order { get { return Model as Order; } }
    }
}
