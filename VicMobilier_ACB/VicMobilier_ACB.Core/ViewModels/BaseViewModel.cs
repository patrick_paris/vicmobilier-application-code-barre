﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB.ViewModels
{
    public abstract class BaseViewModel
    {
        public BaseModel Model { get; set; }

        public int ID { get { return Model.ID; }  }
    }
}
