﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB.ViewModels
{
    public class PlantViewModel : BaseViewModel
    {
        public PlantViewModel(Plant plant)
        {
            Model = plant;
        }

        public string Name {
            get
            {
                var plant = Model as Plant;
                return plant == null ? string.Empty : plant.Name;
            }
        }
    }
}
