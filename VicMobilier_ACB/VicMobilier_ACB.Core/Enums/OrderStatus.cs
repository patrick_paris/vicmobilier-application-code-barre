﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB
{
    public enum OrderStatus
    {
        Packed = 1,
        Shipping = 2,
        PartiallyShipped = 3,
        Shipped = 4,
        Subcontractor = 5,
        Terminated = 6
    }

    public static class OrderStatusExtensions
    {
        public static string ToFriendlyString(this OrderStatus me)
        {
            switch (me)
            {
                case OrderStatus.Packed:
                    return "Emballé";
                case OrderStatus.PartiallyShipped:
                    return "Partiellement expédié";
                case OrderStatus.Shipped:
                    return "Expédié";
                case OrderStatus.Subcontractor:
                    return "Sous-traitant";
                case OrderStatus.Terminated:
                    return "Terminé";
                case OrderStatus.Shipping:
                    return "Expédition en cours";
                default:
                    return string.Empty;
            }
        }
    }
}
