﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB
{
    public enum OrderItemType
    {
        Palet = 1,
        Box = 2
    }

    public static class OrderItemTypeExtensions
    {
        public static string ToString(this OrderItemType me)
        {
            switch (me)
            {
                case OrderItemType.Box:
                    return "Boîte";
                case OrderItemType.Palet:
                    return "Palette";
                default:
                    return string.Empty;
            }
        }
    }
}
