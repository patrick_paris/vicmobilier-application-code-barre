﻿using Loginnove.Sql.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VicMobilier_ACB.ViewModels;

namespace VicMobilier_ACB
{
    public class OrderManager
    {
        private string _ConnectionName;

        public OrderManager(string connectionName)
        {
            _ConnectionName = connectionName;
        }

        public IList<OrderDetailViewModel> GetDetailList(OrderDetailFilter filter)
        {
            var query = "SELECT * FROM OrderDetailView";

            var filterStr = new List<string>();
            var parameters = new List<Parameters>();
            if (filter != null)
            {
                if (filter.PlantID != 0)
                {
                    filterStr.Add("PlantID=@PlantID");
                    parameters.Add(new Parameters("@PlantID", filter.PlantID));
                }
                if (!string.IsNullOrEmpty(filter.OrderNumber))
                {
                    filterStr.Add("OrderNumber=@OrderNumber");
                    parameters.Add(new Parameters("@OrderNumber", filter.OrderNumber));
                }
                if (filter.OrderStatuses != null)
                {
                    if (!filter.OrderStatuses.Any())
                        filter.OrderStatuses.Add(0);
                    filterStr.Add(string.Format("OrderStatus IN ({0})", string.Join(",", filter.OrderStatuses.Select(x => (byte)x))));
                }
                if (filter.PackedDateRange != null)
                {
                    if (filter.PackedDateRange.FromDate.HasValue && filter.PackedDateRange.ToDate.HasValue)
                    {
                        filterStr.Add("LastDatePacked BETWEEN @FromDatePacked AND @ToDatePacked");
                        parameters.Add(new Parameters("@FromDatePacked", filter.PackedDateRange.FromDate.Value));
                        parameters.Add(new Parameters("@ToDatePacked", filter.PackedDateRange.ToDate.Value));
                    }
                    else if (filter.PackedDateRange.FromDate.HasValue)
                    {
                        filterStr.Add("LastDatePacked >= @FromDatePacked");
                        parameters.Add(new Parameters("@FromDatePacked", filter.PackedDateRange.FromDate.Value));
                    }
                    else if (filter.PackedDateRange.ToDate.HasValue)
                    {
                        filterStr.Add("LastDatePacked <= @ToDatePacked");
                        parameters.Add(new Parameters("@ToDatePacked", filter.PackedDateRange.ToDate.Value));
                    }
                }
                if (filter.ShippedDateRange != null)
                {
                    if (filter.ShippedDateRange.FromDate.HasValue && filter.ShippedDateRange.ToDate.HasValue)
                    {
                        filterStr.Add("LastDateShipped BETWEEN @FromDateShipped AND @ToDateShipped");
                        parameters.Add(new Parameters("@FromDateShipped", filter.ShippedDateRange.FromDate.Value));
                        parameters.Add(new Parameters("@ToDateShipped", filter.ShippedDateRange.ToDate.Value));
                    }
                    else if (filter.ShippedDateRange.FromDate.HasValue)
                    {
                        filterStr.Add("LastDateShipped >= @FromDateShipped");
                        parameters.Add(new Parameters("@FromDateShipped", filter.ShippedDateRange.FromDate.Value));
                    }
                    else if (filter.ShippedDateRange.ToDate.HasValue)
                    {
                        filterStr.Add("LastDateShipped <= @ToDateShipped");
                        parameters.Add(new Parameters("@ToDateShipped", filter.ShippedDateRange.ToDate.Value));
                    }
                }
                if (filter.ClosedDateRange != null)
                {
                    if (filter.ClosedDateRange.FromDate.HasValue && filter.ClosedDateRange.ToDate.HasValue)
                    {
                        filterStr.Add("DateClosed BETWEEN @FromDateClosed AND @ToDateClosed");
                        parameters.Add(new Parameters("@FromDateClosed", filter.ClosedDateRange.FromDate.Value));
                        parameters.Add(new Parameters("@ToDateClosed", filter.ClosedDateRange.ToDate.Value));
                    }
                    else if (filter.ClosedDateRange.FromDate.HasValue)
                    {
                        filterStr.Add("DateClosed >= @FromDateClosed");
                        parameters.Add(new Parameters("@FromDateClosed", filter.ClosedDateRange.FromDate.Value));
                    }
                    else if (filter.ClosedDateRange.ToDate.HasValue)
                    {
                        filterStr.Add("DateClosed <= @ToDateClosed");
                        parameters.Add(new Parameters("@ToDateClosed", filter.ClosedDateRange.ToDate.Value));
                    }
                }

                if (filterStr.Any())
                    query += " WHERE " + string.Join(" AND ", filterStr);
            }

            return Loginnove.Sql.Helper.ExecuteReaderToList<OrderDetailViewModel>(_ConnectionName, query, parameters.Any() ? parameters : null);
        }

        public OrderViewModel Add(OrderViewModel orderViewModel)
        {
            var order = orderViewModel.Order;
            if (order == null) return null;

            string query = "INSERT INTO [Order] (Number) VALUES(@Number)";
            List<Parameters> parameters = new List<Parameters>() { new Parameters("@Number", order.Number) };

            Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteNonQuery);

            var lastInserted = Convert.ToInt32(Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, "SELECT IDENT_CURRENT('[Order]')", null, Loginnove.Sql.Enums.QueryTypes.ExecuteScalar));

            return IncludeGetByID(lastInserted, -1);
        }

        public OrderViewModel IncludeGetByID(int id, int plantID)
        {
            OrderViewModel result = null;

            var query = "SELECT * FROM [Order] WHERE ID=@ID";
            var parameters = new List<Parameters>() { new Parameters("@ID", id) };
            var orders = Loginnove.Sql.Helper.ExecuteReaderToList<Order>(_ConnectionName, query, parameters);
            if (orders != null && orders.Count == 1)
            {
                var order = orders.FirstOrDefault();
                order.OrderItems = GetOrderItems(order.ID, plantID);
                result = new OrderViewModel(order);
                foreach (var orderItem in order.OrderItems)
                {
                    result.OrderItems.Add(new OrderItemViewModel(orderItem));
                }
            }

            return result;
        }

        public OrderViewModel IncludeGetByNumber(string number, int plantID)
        {
            OrderViewModel result = null;

            var query = "SELECT * FROM [Order] WHERE Number=@Number";
            var parameters = new List<Parameters>() { new Parameters("@Number", number) };

            var orders = Loginnove.Sql.Helper.ExecuteReaderToList<Order>(_ConnectionName, query, parameters);
            if (orders != null && orders.Count == 1)
            {
                var order = orders.FirstOrDefault();
                order.OrderItems = GetOrderItems(order.ID, plantID);
                result = new OrderViewModel(order);
                if (order.OrderItems != null)
                {
                    foreach (var orderItem in order.OrderItems)
                    {
                        result.OrderItems.Add(new OrderItemViewModel(orderItem));
                    }
                }
            }

            return result;
        }

        private List<OrderItem> GetOrderItems(int orderID, int plantID)
        {
            var query = "SELECT * FROM OrderItem WHERE OrderID = @OrderID";
            var parameters = new List<Parameters>() { new Parameters("@OrderID", orderID) };
            if (plantID != 0)
            {
                query += " AND PlantID = @PlantID";
                parameters.Add(new Parameters("@PlantID", plantID));
            }

            return Loginnove.Sql.Helper.ExecuteReaderToList<OrderItem>(_ConnectionName, query, parameters) ?? new List<OrderItem>();
        }

        public List<OrderSummaryViewModel> GetOrderSummary(int orderID, int plantID)
        {
            var query = string.Format("SELECT * FROM OrderSummaryFunction({0}) WHERE OrderID = @OrderID", plantID);
            var parameters = new List<Parameters>() { new Parameters("@OrderID", orderID) };

            return Loginnove.Sql.Helper.ExecuteReaderToList<OrderSummaryViewModel>(_ConnectionName, query, parameters);
        }

        public OrderItem GetItemByID(int id, int plantID)
        {
            OrderItem result = null;

            var query = "SELECT * FROM OrderItem WHERE ID=@ID";
            var parameters = new List<Parameters>() { new Parameters("@ID", id) };
            if (plantID != 0)
            {
                query += " AND PlantID = @PlantID";
                parameters.Add(new Parameters("@PlantID", plantID));
            }

            var orderItems = Loginnove.Sql.Helper.ExecuteReaderToList<OrderItem>(_ConnectionName, query, parameters);
            if (orderItems != null && orderItems.Count == 1)
            {
                result = orderItems.FirstOrDefault();
            }

            return result;
        }

        public OrderItem PackItem(OrderItem orderItem)
        {
            string query;
            List<Parameters> parameters;

            if (string.IsNullOrEmpty(orderItem.BarCode))
            {
                query = "SELECT [dbo].NextOrderItemBarCode(@OrderID, @PlantID, @ItemType)";
                parameters = new List<Parameters>()
                {
                    new Parameters("@OrderID", orderItem.OrderID),
                    new Parameters("@PlantID", orderItem.PlantID),
                    new Parameters("@ItemType", (byte)orderItem.ItemType)
                };

                orderItem.BarCode = Convert.ToString(Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteScalar));
            }

            query = "INSERT INTO OrderItem(OrderID, PlantID, ItemType, BarCode, DatePacked) VALUES(@OrderID, @PlantID, @ItemType, @BarCode, @DatePacked)";
            parameters = new List<Parameters>()
            {
                new Parameters("@OrderID", orderItem.OrderID),
                new Parameters("@PlantID", orderItem.PlantID),
                new Parameters("@ItemType", orderItem.ItemType),
                new Parameters("@BarCode", orderItem.BarCode),
                new Parameters("@DatePacked", DateTime.Now)
            };
            Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteNonQuery);

            var lastInserted = Convert.ToInt32(Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, "SELECT IDENT_CURRENT('[OrderItem]')", null, Loginnove.Sql.Enums.QueryTypes.ExecuteScalar));

            return GetItemByID(lastInserted, orderItem.PlantID);
        }

        public OrderItem ScanItem(OrderItem orderItem, bool reinitShip = false)
        {
            string query;
            List<Parameters> parameters = new List<Parameters>()
            {
                new Parameters("@ID", orderItem.ID)
            };

            if (reinitShip)
                query = "UPDATE OrderItem SET DateScanned=NULL WHERE ID=@ID";
            else
            { 
                query = "UPDATE OrderItem SET DateScanned=@DateScanned WHERE ID=@ID";
                parameters.Add(new Parameters("@DateScanned", DateTime.Now.Date));
            }

            Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteNonQuery);
            return GetItemByID(orderItem.ID, orderItem.PlantID);
        }

        public OrderItem ShipItem(OrderItem orderItem)
        {
            string query = "UPDATE OrderItem SET DateShipped=@DateShipped WHERE ID=@ID";
            List<Parameters> parameters = new List<Parameters>()
            {
                new Parameters("@DateShipped", DateTime.Now.Date),
                new Parameters("@ID", orderItem.ID)
            };

            Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteNonQuery);
            return GetItemByID(orderItem.ID, orderItem.PlantID);
        }

        public void CloseOrder(string orderNumber)
        {
            string query = "UPDATE [Order] SET DateClosed=@DateClosed, ForcedClose=1 WHERE Number=@Number";
            List<Parameters> parameters = new List<Parameters>()
            {
                new Parameters("@Number", orderNumber),
                new Parameters("@DateClosed", DateTime.Now.Date)
            };

            Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteNonQuery);
        }

        public void RemoveItemByID(int id)
        {
            var query = "DELETE FROM OrderItem WHERE ID = @ID";
            var parameters = new List<Parameters>()
            {
                new Parameters("@ID", id)
            };
            Loginnove.Sql.Helper.ExecuteQuery(_ConnectionName, query, parameters, Loginnove.Sql.Enums.QueryTypes.ExecuteNonQuery);
        }
    }
}
