﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB
{
    public class OrderDetailFilter
    {
        public OrderDetailFilter()
        {
            PackedDateRange = new DateRange();
            ShippedDateRange = new DateRange();
            ClosedDateRange = new DateRange();
        }

        public int PlantID { get; set; }
        public string OrderNumber { get; set; }
        public IList<OrderStatus> OrderStatuses { get; set; }
        public DateRange PackedDateRange { get; set; }
        public DateRange ShippedDateRange { get; set; }
        public DateRange ClosedDateRange { get; set; }
    }

    public class DateRange
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
