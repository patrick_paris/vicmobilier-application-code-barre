﻿using Loginnove.Sql.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VicMobilier_ACB.ViewModels;

namespace VicMobilier_ACB
{
    public class PlantManager
    {
        private string _ConnectionName;

        public PlantManager(string connectionName)
        {
            _ConnectionName = connectionName;
        }


        public IList<PlantViewModel> GetList(bool subContractorOnly = false)
        {
            var result = new List<PlantViewModel>();
            var query = "SELECT * FROM Plant";
            if (subContractorOnly)
                query += " WHERE IsSubcontractor <> 0";
            var plants = Loginnove.Sql.Helper.ExecuteReaderToList<Plant>(_ConnectionName, query, null);
            foreach (var plant in plants)
                result.Add(new PlantViewModel(plant));

            return result;
        }

        public PlantViewModel IncludeGetByID(int id)
        {
            PlantViewModel result = null;

            var query = "SELECT * FROM Plant WHERE ID=@ID";
            var parameters = new List<Parameters>() { new Parameters("@ID", id) };
            var plants = Loginnove.Sql.Helper.ExecuteReaderToList<Plant>(_ConnectionName, query, parameters);
            if (plants != null && plants.Count == 1)
            {
                var plant = plants.FirstOrDefault();
                result = new PlantViewModel(plant);
            }

            return result;
        }

        public PlantViewModel IncludeGetByName(string name)
        {
            PlantViewModel result = null;

            var query = "SELECT * FROM Plant WHERE Name=@Name";
            var parameters = new List<Parameters>() { new Parameters("@Name", name) };
            var plants = Loginnove.Sql.Helper.ExecuteReaderToList<Plant>(_ConnectionName, query, parameters);
            if (plants != null && plants.Count == 1)
            {
                var plant = plants.FirstOrDefault();
                result = new PlantViewModel(plant);
            }

            return result;
        }
    }
}
