﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB
{
    public class OrderItem : BaseModel
    {
        public int OrderID { get; set; }
        public int PlantID { get; set; }
        public Plant Plant { get; set; }
        public OrderItemType ItemType { get; set; }
        public string BarCode { get; set; }
        public DateTime? DatePacked { get; set; }
        public DateTime? DateScanned { get; set; }
        public DateTime? DateShipped { get; set; }
    }
}
