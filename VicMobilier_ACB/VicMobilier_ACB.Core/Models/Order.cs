﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB
{
    public class Order : BaseModel
    {
        public string Number { get; set; }
        public DateTime? DateClosed { get; set; }
        public IList<OrderItem> OrderItems { get; set; }
    }
}
