﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicMobilier_ACB
{
    public abstract class BaseModel
    {
        [Key]
        public int ID { get; set; }
        [IgnoreProperty]
        public byte[] RowVersion { get; set; }
    }
}
