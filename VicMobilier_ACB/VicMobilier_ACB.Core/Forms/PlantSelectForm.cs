﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VicMobilier_ACB.ViewModels;

namespace VicMobilier_ACB
{
    public partial class PlantSelectForm : Form
    {
        public PlantSelectForm(PlantManager plantManager, bool subContractorOnly)
        {
            InitializeComponent();

            plantViewModelBindingSource.DataSource = plantManager.GetList(subContractorOnly);
        }

        public int SelectPlantID(IWin32Window owner)
        {
            var plants = plantViewModelBindingSource.DataSource as IList<PlantViewModel>;

            if (plants.Count == 1)
            {
                return plants.FirstOrDefault().ID;
            }
            else
            {
                if (this.ShowDialog(owner) == DialogResult.OK)
                {
                    return (int)plantComboBox.SelectedValue;
                }
                else
                    return -1;
            }
        }

        public string SelectPlantName(IWin32Window owner)
        {
            var plants = plantViewModelBindingSource.DataSource as IList<PlantViewModel>;

            if (plants.Count == 1)
            {
                return plants.FirstOrDefault().Name;
            }
            else
            {
                if (this.ShowDialog(owner) == DialogResult.OK)
                {
                    return plantComboBox.Text;
                }
                else
                    return string.Empty;
            }
        }
    }
}
