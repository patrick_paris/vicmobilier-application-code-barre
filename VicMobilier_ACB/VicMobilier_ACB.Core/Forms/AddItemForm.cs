﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VicMobilier_ACB.ViewModels;

namespace VicMobilier_ACB.Core
{
    public partial class AddItemForm : Form
    {
        private OrderManager _OrderManager;
        private OrderViewModel _Order;
        private PlantViewModel _Plant;

        public AddItemForm(OrderManager orderManager, OrderViewModel order, PlantViewModel plant)
        {
            InitializeComponent();

            var enums = new List<EnumClass>()
            {
                new EnumClass(1, "Palette"),
                new EnumClass(2, "Boîte"),
            };

            orderItemTypeComboBox.DataSource = enums;

            _OrderManager = orderManager;
            _Order = order;
            _Plant = plant;

            quantityTextBox.Select();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            int quantity;
            Int32.TryParse(quantityTextBox.Text, out quantity);

            if (quantity == 0) return;

            if (_Order.ID == 0)
                _Order = _OrderManager.Add(_Order);

            for (var i = 1; i <= quantity; i++)
            {
                OrderItem orderItem = new OrderItem()
                {
                    ItemType = (OrderItemType)orderItemTypeComboBox.SelectedValue,
                    OrderID = _Order.Model.ID,
                    PlantID = _Plant.Model.ID
                };
                _OrderManager.PackItem(orderItem);
            }
        }
    }

    public class EnumClass
    {
        public EnumClass(int value, string display)
        {
            Value = value;
            Display = display;
        }

        public int Value { get; set; }
        public string Display { get; set; }
    }
}
