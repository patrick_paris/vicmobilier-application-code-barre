﻿namespace VicMobilier_ACB.Core
{
    partial class ShippingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.confirmShippingButton = new System.Windows.Forms.Button();
            this.pictureButton = new System.Windows.Forms.Button();
            this.topPanel = new System.Windows.Forms.Panel();
            this.allPlantCheckBox = new System.Windows.Forms.CheckBox();
            this.itemTextBox = new System.Windows.Forms.TextBox();
            this.itemLabel = new System.Windows.Forms.Label();
            this.orderNumberTextBox = new System.Windows.Forms.TextBox();
            this.orderNumberLabel = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.orderItemGridView = new System.Windows.Forms.DataGridView();
            this.OrderItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Scanned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.orderSummaryGridView = new System.Windows.Forms.DataGridView();
            this.SummaryOrderItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SummaryPackedCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SummaryScannedCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bottomPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderItemGridView)).BeginInit();
            this.rightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderSummaryGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.confirmShippingButton);
            this.bottomPanel.Controls.Add(this.pictureButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 433);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(652, 35);
            this.bottomPanel.TabIndex = 1;
            // 
            // confirmShippingButton
            // 
            this.confirmShippingButton.Location = new System.Drawing.Point(273, 6);
            this.confirmShippingButton.Name = "confirmShippingButton";
            this.confirmShippingButton.Size = new System.Drawing.Size(75, 23);
            this.confirmShippingButton.TabIndex = 1;
            this.confirmShippingButton.Text = "Confirmer";
            this.confirmShippingButton.UseVisualStyleBackColor = true;
            this.confirmShippingButton.Click += new System.EventHandler(this.confirmShippingButton_Click);
            // 
            // pictureButton
            // 
            this.pictureButton.Location = new System.Drawing.Point(7, 6);
            this.pictureButton.Name = "pictureButton";
            this.pictureButton.Size = new System.Drawing.Size(75, 23);
            this.pictureButton.TabIndex = 0;
            this.pictureButton.Text = "Photo";
            this.pictureButton.UseVisualStyleBackColor = true;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.allPlantCheckBox);
            this.topPanel.Controls.Add(this.itemTextBox);
            this.topPanel.Controls.Add(this.itemLabel);
            this.topPanel.Controls.Add(this.orderNumberTextBox);
            this.topPanel.Controls.Add(this.orderNumberLabel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(652, 58);
            this.topPanel.TabIndex = 5;
            // 
            // allPlantCheckBox
            // 
            this.allPlantCheckBox.AutoSize = true;
            this.allPlantCheckBox.Location = new System.Drawing.Point(271, 8);
            this.allPlantCheckBox.Name = "allPlantCheckBox";
            this.allPlantCheckBox.Size = new System.Drawing.Size(183, 17);
            this.allPlantCheckBox.TabIndex = 8;
            this.allPlantCheckBox.Text = "Tous les items / Toues les usines";
            this.allPlantCheckBox.UseVisualStyleBackColor = true;
            this.allPlantCheckBox.CheckedChanged += new System.EventHandler(this.allPlantCheckBox_CheckedChanged);
            // 
            // itemTextBox
            // 
            this.itemTextBox.Location = new System.Drawing.Point(93, 32);
            this.itemTextBox.Name = "itemTextBox";
            this.itemTextBox.Size = new System.Drawing.Size(148, 20);
            this.itemTextBox.TabIndex = 7;
            this.itemTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.itemTextBox_KeyDown);
            // 
            // itemLabel
            // 
            this.itemLabel.AutoSize = true;
            this.itemLabel.Location = new System.Drawing.Point(12, 35);
            this.itemLabel.Name = "itemLabel";
            this.itemLabel.Size = new System.Drawing.Size(27, 13);
            this.itemLabel.TabIndex = 6;
            this.itemLabel.Text = "Item";
            // 
            // orderNumberTextBox
            // 
            this.orderNumberTextBox.Location = new System.Drawing.Point(93, 6);
            this.orderNumberTextBox.Name = "orderNumberTextBox";
            this.orderNumberTextBox.Size = new System.Drawing.Size(148, 20);
            this.orderNumberTextBox.TabIndex = 5;
            this.orderNumberTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orderNumberTextBox_KeyDown);
            this.orderNumberTextBox.Validated += new System.EventHandler(this.orderNumberTextBox_Validated);
            // 
            // orderNumberLabel
            // 
            this.orderNumberLabel.AutoSize = true;
            this.orderNumberLabel.Location = new System.Drawing.Point(12, 9);
            this.orderNumberLabel.Name = "orderNumberLabel";
            this.orderNumberLabel.Size = new System.Drawing.Size(60, 13);
            this.orderNumberLabel.TabIndex = 4;
            this.orderNumberLabel.Text = "Commande";
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.orderItemGridView);
            this.mainPanel.Controls.Add(this.splitter1);
            this.mainPanel.Controls.Add(this.rightPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 58);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(652, 375);
            this.mainPanel.TabIndex = 8;
            // 
            // orderItemGridView
            // 
            this.orderItemGridView.AllowUserToAddRows = false;
            this.orderItemGridView.AllowUserToDeleteRows = false;
            this.orderItemGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderItemGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OrderItemType,
            this.BarCode,
            this.Scanned});
            this.orderItemGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderItemGridView.Location = new System.Drawing.Point(0, 0);
            this.orderItemGridView.Name = "orderItemGridView";
            this.orderItemGridView.ReadOnly = true;
            this.orderItemGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.orderItemGridView.Size = new System.Drawing.Size(348, 375);
            this.orderItemGridView.TabIndex = 5;
            // 
            // OrderItemType
            // 
            this.OrderItemType.DataPropertyName = "OrderItemType";
            this.OrderItemType.HeaderText = "Type";
            this.OrderItemType.Name = "OrderItemType";
            this.OrderItemType.ReadOnly = true;
            // 
            // BarCode
            // 
            this.BarCode.DataPropertyName = "BarCode";
            this.BarCode.HeaderText = "Code barre";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            // 
            // Scanned
            // 
            this.Scanned.DataPropertyName = "Scanned";
            this.Scanned.HeaderText = "Saisi";
            this.Scanned.Name = "Scanned";
            this.Scanned.ReadOnly = true;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(348, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 375);
            this.splitter1.TabIndex = 8;
            this.splitter1.TabStop = false;
            // 
            // rightPanel
            // 
            this.rightPanel.Controls.Add(this.orderSummaryGridView);
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightPanel.Location = new System.Drawing.Point(351, 0);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(301, 375);
            this.rightPanel.TabIndex = 7;
            // 
            // orderSummaryGridView
            // 
            this.orderSummaryGridView.AllowUserToAddRows = false;
            this.orderSummaryGridView.AllowUserToDeleteRows = false;
            this.orderSummaryGridView.AllowUserToResizeRows = false;
            this.orderSummaryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderSummaryGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SummaryOrderItemType,
            this.SummaryPackedCount,
            this.SummaryScannedCount});
            this.orderSummaryGridView.Dock = System.Windows.Forms.DockStyle.Top;
            this.orderSummaryGridView.Location = new System.Drawing.Point(0, 0);
            this.orderSummaryGridView.Name = "orderSummaryGridView";
            this.orderSummaryGridView.ReadOnly = true;
            this.orderSummaryGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.orderSummaryGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.orderSummaryGridView.Size = new System.Drawing.Size(301, 149);
            this.orderSummaryGridView.TabIndex = 7;
            // 
            // SummaryOrderItemType
            // 
            this.SummaryOrderItemType.DataPropertyName = "OrderItemType";
            this.SummaryOrderItemType.Frozen = true;
            this.SummaryOrderItemType.HeaderText = "Type";
            this.SummaryOrderItemType.Name = "SummaryOrderItemType";
            this.SummaryOrderItemType.ReadOnly = true;
            // 
            // SummaryPackedCount
            // 
            this.SummaryPackedCount.DataPropertyName = "PackedCount";
            this.SummaryPackedCount.Frozen = true;
            this.SummaryPackedCount.HeaderText = "Emballé";
            this.SummaryPackedCount.Name = "SummaryPackedCount";
            this.SummaryPackedCount.ReadOnly = true;
            this.SummaryPackedCount.Width = 75;
            // 
            // SummaryScannedCount
            // 
            this.SummaryScannedCount.DataPropertyName = "ScannedCount";
            this.SummaryScannedCount.HeaderText = "Saisi";
            this.SummaryScannedCount.Name = "SummaryScannedCount";
            this.SummaryScannedCount.ReadOnly = true;
            this.SummaryScannedCount.Width = 75;
            // 
            // ShippingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 468);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.bottomPanel);
            this.Name = "ShippingForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Emballage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ShippingForm_FormClosing);
            this.bottomPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderItemGridView)).EndInit();
            this.rightPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderSummaryGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TextBox orderNumberTextBox;
        private System.Windows.Forms.Label orderNumberLabel;
        private System.Windows.Forms.Button pictureButton;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.DataGridView orderItemGridView;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel rightPanel;
        private System.Windows.Forms.DataGridView orderSummaryGridView;
        private System.Windows.Forms.CheckBox allPlantCheckBox;
        private System.Windows.Forms.TextBox itemTextBox;
        private System.Windows.Forms.Label itemLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn SummaryOrderItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn SummaryPackedCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SummaryScannedCount;
        private System.Windows.Forms.Button confirmShippingButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Scanned;
    }
}

