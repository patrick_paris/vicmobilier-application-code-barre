﻿namespace VicMobilier_ACB.Core
{
    partial class PackingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackingForm));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.OrderItemViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.printButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.topPanel = new System.Windows.Forms.Panel();
            this.orderNumberTextBox = new System.Windows.Forms.TextBox();
            this.orderNumberLabel = new System.Windows.Forms.Label();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.checkAllButton = new System.Windows.Forms.ToolStripButton();
            this.uncheckAllButton = new System.Windows.Forms.ToolStripButton();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.orderItemGridView = new System.Windows.Forms.DataGridView();
            this.OrderItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.rightPanel = new System.Windows.Forms.Panel();
            this.barCodeReportViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.orderSummaryGridView = new System.Windows.Forms.DataGridView();
            this.SummaryOrderItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SummaryPackedCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.OrderItemViewModelBindingSource)).BeginInit();
            this.bottomPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderItemGridView)).BeginInit();
            this.rightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderSummaryGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // OrderItemViewModelBindingSource
            // 
            this.OrderItemViewModelBindingSource.DataSource = typeof(VicMobilier_ACB.ViewModels.OrderItemViewModel);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.printButton);
            this.bottomPanel.Controls.Add(this.removeButton);
            this.bottomPanel.Controls.Add(this.addButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 416);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(591, 35);
            this.bottomPanel.TabIndex = 1;
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(170, 6);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 2;
            this.printButton.Text = "Imprimer";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(88, 6);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 1;
            this.removeButton.Text = "Retirer";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(7, 6);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Ajouter";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.orderNumberTextBox);
            this.topPanel.Controls.Add(this.orderNumberLabel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(591, 33);
            this.topPanel.TabIndex = 5;
            // 
            // orderNumberTextBox
            // 
            this.orderNumberTextBox.Location = new System.Drawing.Point(93, 6);
            this.orderNumberTextBox.Name = "orderNumberTextBox";
            this.orderNumberTextBox.Size = new System.Drawing.Size(148, 20);
            this.orderNumberTextBox.TabIndex = 5;
            this.orderNumberTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orderNumberTextBox_KeyDown);
            this.orderNumberTextBox.Validated += new System.EventHandler(this.orderNumberTextBox_Validated);
            // 
            // orderNumberLabel
            // 
            this.orderNumberLabel.AutoSize = true;
            this.orderNumberLabel.Location = new System.Drawing.Point(12, 9);
            this.orderNumberLabel.Name = "orderNumberLabel";
            this.orderNumberLabel.Size = new System.Drawing.Size(60, 13);
            this.orderNumberLabel.TabIndex = 4;
            this.orderNumberLabel.Text = "Commande";
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkAllButton,
            this.uncheckAllButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 33);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(591, 31);
            this.mainToolStrip.TabIndex = 7;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // checkAllButton
            // 
            this.checkAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.checkAllButton.Image = ((System.Drawing.Image)(resources.GetObject("checkAllButton.Image")));
            this.checkAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.checkAllButton.Name = "checkAllButton";
            this.checkAllButton.Size = new System.Drawing.Size(28, 28);
            this.checkAllButton.Text = "toolStripButton1";
            this.checkAllButton.Click += new System.EventHandler(this.checkAllButton_Click);
            // 
            // uncheckAllButton
            // 
            this.uncheckAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uncheckAllButton.Image = ((System.Drawing.Image)(resources.GetObject("uncheckAllButton.Image")));
            this.uncheckAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uncheckAllButton.Name = "uncheckAllButton";
            this.uncheckAllButton.Size = new System.Drawing.Size(28, 28);
            this.uncheckAllButton.Text = "toolStripButton2";
            this.uncheckAllButton.Click += new System.EventHandler(this.uncheckAllButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.orderItemGridView);
            this.mainPanel.Controls.Add(this.splitter1);
            this.mainPanel.Controls.Add(this.rightPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 64);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(591, 352);
            this.mainPanel.TabIndex = 8;
            // 
            // orderItemGridView
            // 
            this.orderItemGridView.AllowUserToAddRows = false;
            this.orderItemGridView.AllowUserToDeleteRows = false;
            this.orderItemGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderItemGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OrderItemType,
            this.BarCode,
            this.Selected});
            this.orderItemGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderItemGridView.Location = new System.Drawing.Point(0, 0);
            this.orderItemGridView.Name = "orderItemGridView";
            this.orderItemGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.orderItemGridView.Size = new System.Drawing.Size(359, 352);
            this.orderItemGridView.TabIndex = 5;
            // 
            // OrderItemType
            // 
            this.OrderItemType.DataPropertyName = "OrderItemType";
            this.OrderItemType.HeaderText = "Type";
            this.OrderItemType.Name = "OrderItemType";
            this.OrderItemType.ReadOnly = true;
            // 
            // BarCode
            // 
            this.BarCode.DataPropertyName = "BarCode";
            this.BarCode.HeaderText = "Code barre";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            // 
            // Selected
            // 
            this.Selected.DataPropertyName = "Selected";
            this.Selected.HeaderText = "Sélection";
            this.Selected.Name = "Selected";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(359, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 352);
            this.splitter1.TabIndex = 8;
            this.splitter1.TabStop = false;
            // 
            // rightPanel
            // 
            this.rightPanel.Controls.Add(this.barCodeReportViewer);
            this.rightPanel.Controls.Add(this.orderSummaryGridView);
            this.rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightPanel.Location = new System.Drawing.Point(362, 0);
            this.rightPanel.Name = "rightPanel";
            this.rightPanel.Size = new System.Drawing.Size(229, 352);
            this.rightPanel.TabIndex = 7;
            // 
            // barCodeReportViewer
            // 
            this.barCodeReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "OrderItemDataSet";
            reportDataSource1.Value = this.OrderItemViewModelBindingSource;
            this.barCodeReportViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.barCodeReportViewer.LocalReport.ReportEmbeddedResource = "VicMobilier_ACB.Report.BarCodeReportLabel.rdlc";
            this.barCodeReportViewer.Location = new System.Drawing.Point(0, 149);
            this.barCodeReportViewer.Name = "barCodeReportViewer";
            this.barCodeReportViewer.Size = new System.Drawing.Size(229, 203);
            this.barCodeReportViewer.TabIndex = 8;
            // 
            // orderSummaryGridView
            // 
            this.orderSummaryGridView.AllowUserToAddRows = false;
            this.orderSummaryGridView.AllowUserToDeleteRows = false;
            this.orderSummaryGridView.AllowUserToResizeRows = false;
            this.orderSummaryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderSummaryGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SummaryOrderItemType,
            this.SummaryPackedCount});
            this.orderSummaryGridView.Dock = System.Windows.Forms.DockStyle.Top;
            this.orderSummaryGridView.Location = new System.Drawing.Point(0, 0);
            this.orderSummaryGridView.Name = "orderSummaryGridView";
            this.orderSummaryGridView.ReadOnly = true;
            this.orderSummaryGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.orderSummaryGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.orderSummaryGridView.Size = new System.Drawing.Size(229, 149);
            this.orderSummaryGridView.TabIndex = 7;
            // 
            // SummaryOrderItemType
            // 
            this.SummaryOrderItemType.DataPropertyName = "OrderItemType";
            this.SummaryOrderItemType.Frozen = true;
            this.SummaryOrderItemType.HeaderText = "Type";
            this.SummaryOrderItemType.Name = "SummaryOrderItemType";
            this.SummaryOrderItemType.ReadOnly = true;
            // 
            // SummaryPackedCount
            // 
            this.SummaryPackedCount.DataPropertyName = "PackedCount";
            this.SummaryPackedCount.Frozen = true;
            this.SummaryPackedCount.HeaderText = "Emballé";
            this.SummaryPackedCount.Name = "SummaryPackedCount";
            this.SummaryPackedCount.ReadOnly = true;
            this.SummaryPackedCount.Width = 75;
            // 
            // PackingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 451);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.bottomPanel);
            this.Name = "PackingForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Emballage";
            this.Load += new System.EventHandler(this.PackingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OrderItemViewModelBindingSource)).EndInit();
            this.bottomPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderItemGridView)).EndInit();
            this.rightPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderSummaryGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TextBox orderNumberTextBox;
        private System.Windows.Forms.Label orderNumberLabel;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton checkAllButton;
        private System.Windows.Forms.ToolStripButton uncheckAllButton;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.DataGridView orderItemGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel rightPanel;
        private System.Windows.Forms.DataGridView orderSummaryGridView;
        private Microsoft.Reporting.WinForms.ReportViewer barCodeReportViewer;
        private System.Windows.Forms.BindingSource OrderItemViewModelBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn SummaryOrderItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn SummaryPackedCount;
    }
}

