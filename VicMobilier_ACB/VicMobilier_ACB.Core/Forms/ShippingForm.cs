﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using VicMobilier_ACB.ViewModels;
using Loginnove.Mails.Models;

namespace VicMobilier_ACB.Core
{
    public partial class ShippingForm : Form
    {
        private OrderViewModel _CurrentOrder;
        private PlantViewModel _CurrentPlant;

        private PlantManager _PlantManager;
        private OrderManager _OrderManager;

        private string _PlantName;

        public ShippingForm(string plantName, PlantManager plantManager, OrderManager orderManager, string currentOrderNumber = "")
        {
            InitializeComponent();
            this.orderItemGridView.AutoGenerateColumns = false;
            this.orderSummaryGridView.AutoGenerateColumns = false;

            _PlantManager = plantManager;
            _OrderManager = orderManager;

            _PlantName = plantName;

            if (plantName == "admin")
                _CurrentPlant = new PlantViewModel(new Plant() {ID = 0, Name = "Admin" } );
            else
                _CurrentPlant = _PlantManager.IncludeGetByName(plantName);

            var plant = _CurrentPlant.Model as Plant;
            if (plant != null)
                this.Text = string.Format("Emballage - {0}", plant.Name);

            orderNumberTextBox.Text = currentOrderNumber;
            RefreshOrder();
        }

        public void RefreshOrder()
        {
            if (_CurrentOrder != null && _CurrentOrder.Order.Number != orderNumberTextBox.Text)
                CheckUnconfirmed();

            _CurrentOrder = null;
            this.orderItemGridView.DataSource = null;
            this.orderSummaryGridView.DataSource = null;
            var orderNumber = orderNumberTextBox.Text;
            var order = _OrderManager.IncludeGetByNumber(orderNumber, _CurrentPlant.ID);
            if (order != null)
            {
                _CurrentOrder = order;
                confirmShippingButton.Enabled = false;
                if (order.OrderItems != null && order.OrderItems.Any())
                {
                    this.orderItemGridView.DataSource = order.OrderItems;
                    this.orderSummaryGridView.DataSource = _OrderManager.GetOrderSummary(order.Model.ID, _CurrentPlant.ID);

                    if (!order.OrderItems.Any(x => x.DateShipped != null))
                        confirmShippingButton.Enabled = true;
                }
            }
        }

        private void orderNumberTextBox_Validated(object sender, EventArgs e)
        {
            RefreshOrder();
        }

        private void orderNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RefreshOrder();
                itemTextBox.Select();
            }
        }

        public void ScanItem()
        {
            var orderItemViewModels = orderItemGridView.DataSource as IList<OrderItemViewModel>;

            OrderItemViewModel orderItemViewModel = orderItemViewModels.FirstOrDefault(x => x.BarCode.ToUpper() == itemTextBox.Text.ToUpper());
            if (orderItemViewModel == null) return;

            if (!orderItemViewModel.Scanned)
                _OrderManager.ScanItem(orderItemViewModel.Model as OrderItem);
            else if (orderItemViewModel.Scanned)
                _OrderManager.ScanItem(orderItemViewModel.Model as OrderItem, true);

            RefreshOrder();

            itemTextBox.Text = "";
        }

        private void itemTextBox_Validated(object sender, KeyEventArgs e)
        {
            ScanItem();
        }

        private void itemTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ScanItem();
        }

        public void ShipItems()
        {
            var orderItemViewModels = orderItemGridView.DataSource as IList<OrderItemViewModel>;

            foreach (var orderItemViewModel in orderItemViewModels)
            {
                _OrderManager.ShipItem(orderItemViewModel.Model as OrderItem);
            }

            RefreshOrder();
        }

        private void allPlantCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (sender as CheckBox);
            if (checkBox == null) return;

            if (checkBox.Checked)
                _CurrentPlant = new PlantViewModel(new Plant() { ID = 0, Name = "Admin" });
            else
                _CurrentPlant = _PlantManager.IncludeGetByName(_PlantName);

            RefreshOrder();
        }

        private void confirmShippingButton_Click(object sender, EventArgs e)
        {
            var orderItemViewModels = orderItemGridView.DataSource as IList<OrderItemViewModel>;

            var unscanned = orderItemViewModels.Where(x => !x.Scanned);
            if (unscanned.Count() > 0)
            {
                if (MessageBox.Show(this, "Certains items n'ont pas été saisi (scan).\r\nConfirmer quand même l'expédition ?", "Confirmation d'expédition", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ShipItems();
                    SendPartialShipMail();
                    CheckForSubContractor();
                }
            }
            else
            {
                ShipItems();
                CheckForSubContractor();
            }

        }

        private void SendPartialShipMail()
        {
            var message = string.Format("L'expédition pour la commande '{0}' a été confirmée, mais certain item n'ont pas été saisi: <br>", _CurrentOrder.Order.Number);

            var orderItemViewModels = orderItemGridView.DataSource as IList<OrderItemViewModel>;

            foreach (var item in orderItemViewModels.Where(x => !x.Scanned))
            {
                message += string.Format("<br>Code barre '{0}'", item.BarCode);
            }

            Mail mail = new Mail()
            {
                Subject = string.Format("Expédition partielle commande '{0}'", _CurrentOrder.Order.Number),
                To = new List<string>(ConfigurationManager.AppSettings["MailPartialShipSendTo"].Split(';').ToList()),
                From = ConfigurationManager.AppSettings["MailFrom"],
                FromName = ConfigurationManager.AppSettings["MailFromName"],
                Host = ConfigurationManager.AppSettings["SMTPHost"],
                Message = message
            };

            Loginnove.Mails.MailSender.SendMail(mail);
        }

        private void CheckForSubContractor()
        {
            var filter = new OrderDetailFilter()
            {
                OrderNumber = _CurrentOrder.Order.Number,
                PlantID = 0
            };

            var items = _OrderManager.GetDetailList(filter).Where(x => x.PlantName != _CurrentPlant.Name);
            var otherPlanToShip = items.Where(x => (OrderStatus)x.OrderStatus == OrderStatus.Packed || (OrderStatus)x.OrderStatus == OrderStatus.Shipping);
            if (!otherPlanToShip.Any())
            {
                var message = string.Format("Au moins un item d'usine sous-traitante doit être expédié pour la commande '{0}':<br>", _CurrentOrder.Order.Number);

                foreach (var plantName in items.Where(x => (OrderStatus)x.OrderStatus == OrderStatus.Subcontractor).GroupBy(x => x.PlantName))
                {
                    message += string.Format("<br>Usine '{0}'", plantName.Key);
                    var order = _OrderManager.IncludeGetByNumber(_CurrentOrder.Order.Number, _PlantManager.IncludeGetByName(plantName.Key).ID);
                    foreach (var orderItem in order.OrderItems.OrderBy(x => x.BarCode))
                        message += string.Format("<br>&nbsp;&nbsp;&nbsp;&nbsp;Code barre '{0}'", orderItem.BarCode);
                }

                Mail mail = new Mail()
                {
                    Subject = string.Format("Item en sous-traitance pour commande '{0}'", _CurrentOrder.Order.Number),
                    To = new List<string>(ConfigurationManager.AppSettings["MailSubContractorSendTo"].Split(';').ToList()),
                    From = ConfigurationManager.AppSettings["MailFrom"],
                    FromName = ConfigurationManager.AppSettings["MailFromName"],
                    Host = ConfigurationManager.AppSettings["SMTPHost"],
                    Message = message                    
                };

                Loginnove.Mails.MailSender.SendMail(mail);
            }
        }

        private void ShippingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CheckUnconfirmed();
        }

        private void CheckUnconfirmed()
        {
            var orderItemViewModels = orderItemGridView.DataSource as IList<OrderItemViewModel>;

            if (orderItemViewModels.Any(x => x.Scanned && x.DateShipped == null))
            {
                if (MessageBox.Show(this, "Certains items ont été saisis (scan), mais non confirmé.\r\nConfirmer l'expédition avant de quitter ?", "Confirmation d'expédition", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    ShipItems();
            }
        }
    }
}
