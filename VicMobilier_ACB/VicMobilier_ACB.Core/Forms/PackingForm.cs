﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using VicMobilier_ACB.ViewModels;
using System.Drawing.Printing;

namespace VicMobilier_ACB.Core
{
    public partial class PackingForm : Form
    {
        private OrderViewModel _CurrentOrder;
        private PlantViewModel _CurrentPlant;
        private PrinterSettings _PrinterSettings;

        private PlantManager _PlantManager;
        private OrderManager _OrderManager;

        public PackingForm(string plantName, PlantManager plantManager, OrderManager orderManager, string currentOrderNumber = "")
        {
            InitializeComponent();
            this.orderItemGridView.AutoGenerateColumns = false;
            this.orderSummaryGridView.AutoGenerateColumns = false;

            _PlantManager = plantManager;
            _OrderManager = orderManager;
            if (plantName == "admin")
                _CurrentPlant = new PlantViewModel(new Plant() {ID = 0, Name = "Admin" } );
            else
                _CurrentPlant = _PlantManager.IncludeGetByName(plantName);

            var plant = _CurrentPlant.Model as Plant;
            if (plant != null)
                this.Text = string.Format("Emballage - {0}", plant.Name);

            orderNumberTextBox.Text = currentOrderNumber;
            RefreshOrder();
        }

        public void RefreshOrder()
        {
            _CurrentOrder = null;
            this.orderItemGridView.DataSource = null;
            this.orderSummaryGridView.DataSource = null;
            var orderNumber = orderNumberTextBox.Text;
            var order = _OrderManager.IncludeGetByNumber(orderNumber, _CurrentPlant.ID);
            if (order != null)
            {
                _CurrentOrder = order;
                if (order.OrderItems != null && order.OrderItems.Any())
                { 
                    this.orderItemGridView.DataSource = order.OrderItems;
                    this.orderSummaryGridView.DataSource = _OrderManager.GetOrderSummary(order.Model.ID, _CurrentPlant.ID);
                    CheckAll(true);
                }
            }
        }

        private void orderNumberTextBox_Validated(object sender, EventArgs e)
        {
            RefreshOrder();
        }

        private void orderNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                RefreshOrder();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (_CurrentOrder == null)
                _CurrentOrder = new OrderViewModel(new Order() { ID = 0, Number = orderNumberTextBox.Text });

            if (new AddItemForm(_OrderManager, _CurrentOrder, _CurrentPlant).ShowDialog(this) == DialogResult.OK)
            {
                RefreshOrder();
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (_CurrentOrder == null) return;

            if (MessageBox.Show(this, "Supprimer items sélectionnés ?", "Suppression d'item", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var orderItems = orderItemGridView.DataSource as IList<OrderItemViewModel>;

                foreach (var orderItem in orderItems.Where(x => x.Selected))
                {
                    _OrderManager.RemoveItemByID(orderItem.Model.ID);
                }

                RefreshOrder();
            }
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            if (_CurrentOrder == null) return;

            var orderItems = orderItemGridView.DataSource as IList<OrderItemViewModel>;

            OrderItemViewModelBindingSource.DataSource = orderItems.Where(x => x.Selected);
            barCodeReportViewer.RefreshReport();
        }

        private void checkAllButton_Click(object sender, EventArgs e)
        {
            CheckAll(true);
        }

        private void uncheckAllButton_Click(object sender, EventArgs e)
        {
            CheckAll(false);
        }

        private void CheckAll(bool isChecked)
        {
            if (_CurrentOrder == null) return;

            orderItemGridView.EndEdit();
            var orderItems = orderItemGridView.DataSource as IList<OrderItemViewModel>;
            foreach (var orderItem in orderItems)
            {
                orderItem.Selected = isChecked;
            }
            orderItemGridView.Invalidate();
        }

        private void PackingForm_Load(object sender, EventArgs e)
        {

        }
    }
}
