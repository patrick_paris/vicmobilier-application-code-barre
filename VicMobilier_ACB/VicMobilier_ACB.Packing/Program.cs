﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using VicMobilier_ACB.Core;

namespace VicMobilier_ACB.Packing
{
    static class Program
    {
        public static PlantManager PlantManager;
        public static OrderManager OrderManager;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            PlantManager = new PlantManager("DefaultConnection");
            OrderManager = new OrderManager("DefaultConnection");

            var plantName = ConfigurationManager.AppSettings["CurrentPlantName"];

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PackingForm(plantName, PlantManager, OrderManager));
        }
    }
}
